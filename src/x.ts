import prompts from "prompts";

console.clear();
console.log("我欲修仙 by 浮光掠影 - v0.0.1\n");
function sleep(time: number) {
  return new Promise<void>((res) => {
    setTimeout(() => {
      res();
    }, time);
  });
}
(async () => {
  const init = await prompts([
    {
      type: "text",
      name: "name",
      message: "请输入你的名字",
    },
    {
      type: "toggle",
      name: "sex",
      message: "请选择你的性别",
      initial: true,
      active: "须眉",
      inactive: "巾帼",
    },
    {
      type: "multiselect",
      name: "talent",
      message: "请选择三个先天气运",
      instructions: "",
      choices: [
        {
          title: "一诺千金",
          talent: "0",
          description: "你有一个对你非常忠贞的道侣",
        },
        {
          title: "鬼谷外门",
          talent: "1",
          description: "攻击力+10，悟性+30，声望+150，魅力+100",
        },
        {
          title: "武圣转世",
          talent: "2",
          description: "攻击力+10，防御力+5，悟性+20，声望+100",
        },
        {
          title: "人族圣体",
          talent: "3",
          description: "攻击力+8，悟性+10，幸运+10，体力上限+150，魅力+100",
        },
        {
          title: "剑痴",
          talent: "4",
          description: "剑法资质+30，剑法熟练提升速度+50%",
        },
        { title: "倾国倾城", talent: "5", description: "魅力锁定为900" },
        {
          title: "天妒英才",
          talent: "6",
          description: "悟性+30，获的修为+20%，寿命上限-20，健康上限-20",
        },
        {
          title: "冷血",
          talent: "7",
          description: "攻击力+6，会心+20，魅力-100",
        },
        { title: "白虹贯日", talent: "8", description: "枪、指法资质+30" },
      ],
      max: 3,
      min: 3,
      hint: "上下切换选项，回车以确认",
    },
  ]);
  console.clear();
  console.log(init);
  console.log("盘古正在开天辟地中...\n");
  await sleep(2000);
  console.log(
    "你幼时曾见过两位传奇剑圣为了第一的名号同台展开激烈的较量。此时你便已经明白，剑将会是你一生的挚爱\n"
  );
  await sleep(1700);
  console.log(
    "没人知道你从哪里来，只是身体里流淌的金色血脉时刻提醒着你与常人不同。虽然经脉堵塞让你一度难以修炼，但在一位白衣神王的帮助下，你成功踏上求仙之路\n"
  );
  await sleep(1700);
  console.log(
    "你非常幸运，因为你拥有一个对你至死不渝的青梅竹马。你们发誓不管遇到多大的磨难都绝不会抛弃对方。你们一起经历过生死，一起享受过富贵，今后也将并肩继续前行\n"
  );
  await sleep(1700);

  const res2 = await prompts({
    type: "select",
    name: "action",
    message: "要与 东方凌雪(道侣) 做什么？",
    choices: [
      {
        title: "品茗",
        description: "可增加双方亲密度（消耗精力10）",
        action: 0,
      },
      { title: "双修", description: "可增加双方修为（消耗天数10）", action: 1 },
      { title: "论道", description: "可增加双方道点（消耗精力10）", action: 2 },
    ],
    hint: "上下切换选项，回车以确认",
  });

  let str;
  switch (res2.action) {
    case 0:
      str = "双方亲密度提升了!";
      break;
    case 1:
      str = "双方修为提升了!";
      break;
    default:
      str = "双方道点提升了!";
      break;
  }
  console.log(str);
})();
